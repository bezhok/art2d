using UnityEngine;

namespace src
{
    public class Timer : MonoBehaviour
    {
        private float _startValue;
        public float timeRemaining = 10;
        public bool timerIsRunning;

        private void Start()
        {
            _startValue = timeRemaining;
            timerIsRunning = true;
        }

        public void Reset()
        {
            timerIsRunning = true;
            timeRemaining = _startValue;
        }

        public void Stop()
        {
            timerIsRunning = false;
        }

        private void Update()
        {
            if (timerIsRunning)
            {
                if (timeRemaining > 0)
                {
                    timeRemaining -= Time.deltaTime;
                }
                else
                {
                    timeRemaining = 0;
                    timerIsRunning = false;
                    Messenger.Broadcast(GameEvent.TimeOver);
                }

                Messenger.Broadcast(GameEvent.TimeRemaining, timeRemaining);
            }
        }
    }
}