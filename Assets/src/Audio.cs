﻿using System;
using UnityEngine;

namespace src
{
    public class Audio : MonoBehaviour
    {
        internal static AudioClip boomClip;
        public AudioClip boomClipPrefab;
        
        private void Start()
        {
            boomClip = boomClipPrefab;
        }
    }
}