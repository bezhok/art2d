using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace src.UI
{
    public class GameUIController : MonoBehaviour
    {
        private TextMeshProUGUI _timerTMP;
        public AdMob adMob;
        public Button changeWeaponButton;
        public EndGameWindow endGameWindow;
        public GameObject timerUI;

        private void Start()
        {
            adMob.RequestRewardAd();
            endGameWindow.mainMenuButton.onClick.AddListener(() => { SceneManager.LoadScene("MainMenuScene"); });
            endGameWindow.adButton.onClick.AddListener(() =>
            {
                adMob.ShowRewardAd();
                Messenger.Broadcast(GameEvent.PlayerWatchedAd);
                endGameWindow.Close();
            });
        
            changeWeaponButton.onClick.AddListener(() => { Messenger.Broadcast(GameEvent.ChangeWeaponId); });
            _timerTMP = timerUI.GetComponent<TextMeshProUGUI>();
        
            Messenger.AddListener<float>(GameEvent.TimeRemaining, OnTimerValueChanged);
            Messenger.AddListener(GameEvent.PlayerLost, () => endGameWindow.Open());
        }

        private void OnTimerValueChanged(float time)
        {
            _timerTMP.SetText(((int) time).ToString());
        }
    }
}