using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace src.UI
{
    public class MainMenuUIController : MonoBehaviour
    {
        private Image _buttonImage;
        private int _colorId;
        private string _nickname = "";
        public Button colorButton;
        public GameObject field;

        private void Start()
        {
            colorButton.onClick.AddListener(ChangeColor);
            _buttonImage = colorButton.GetComponent<Image>();
        }

        public static Color GetColorByID(int id)
        {
            switch (id)
            {
                case 0:
                    return Color.red;
                case 1:
                    return Color.green;
                case 2:
                    return Color.blue;
                default:
                    return Color.red;
            }
        }

        private void ChangeColor()
        {
            _colorId = (_colorId + 1) % 3;

            _buttonImage.color = GetColorByID(_colorId);
        }

        public void StartGame()
        {
            _nickname = field.GetComponent<TMP_InputField>().text;
            if (_nickname == "") return;

            PlayerPrefs.SetString("nickname", _nickname);
            PlayerPrefs.SetInt("colorID", _colorId);

            SceneManager.LoadScene("GameScene");
        }
    }
}