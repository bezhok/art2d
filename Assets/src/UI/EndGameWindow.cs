using UnityEngine;
using UnityEngine.UI;

namespace src.UI
{
    public class EndGameWindow : MonoBehaviour
    {
        public Button adButton;
        public Button mainMenuButton;

        private void Start()
        {
            Close();
        }

        public void Open()
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}