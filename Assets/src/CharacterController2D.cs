﻿using UnityEngine;

namespace src
{
    public class CharacterController2D : MonoBehaviour
    {
        private Rigidbody2D _body;
        private CapsuleCollider2D _box;
        private bool _grounded;
        public float jumpForce = 12.0f;
        public float speed = 4.5f;

        private void Start()
        {
            _box = GetComponent<CapsuleCollider2D>();
            _body = GetComponent<Rigidbody2D>();
        }

        internal void Move(float deltaX)
        {
            var movement = new Vector2(deltaX, _body.velocity.y);
            _body.velocity = movement;

            var max = _box.bounds.max;
            var min = _box.bounds.min;
            var corner1 = new Vector2(max.x, min.y - .1f);
            var corner2 = new Vector2(min.x, min.y - .2f);
            var hit = Physics2D.OverlapArea(corner1, corner2);
        
            _grounded = false;
            if (hit != null) _grounded = true;
            if (deltaX == 0) _grounded = false;
        
            _body.gravityScale = _grounded && Mathf.Approximately(deltaX, 0) ? 0 : 1;
        
            var rotZ = _body.rotation;
            float maxAngle = 50.0f;
            rotZ = Mathf.Clamp(rotZ, -maxAngle, maxAngle);
            _body.rotation = rotZ;
        }

        internal void Jump(Vector2 dir)
        {
            if (_grounded) _body.AddForce(dir * jumpForce, ForceMode2D.Impulse);
        }
    }
}