using UnityEngine;

namespace src.Shooting
{
    public class Ammunition : MonoBehaviour
    {
        public float damage = 60;
        public float startPower = 10;
        private void Start()
        {
            Launch();
        }

        public void Launch()
        {
            GetComponent<Rigidbody2D>().velocity = transform.right * startPower;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            TileDestoyer.instance.Destroy(transform.position, 1);
            gameObject.SetActive(false);
            var tank = other.gameObject.GetComponent<Tank>();
        
            if (tank != null)
            {
                tank.TakeDamage(damage);
            }
        
            AudioSource.PlayClipAtPoint(Audio.boomClip, transform.position, 1);
            Messenger.Broadcast(GameEvent.ShotHit);
            
            Destroy(gameObject);
        }
    }
}