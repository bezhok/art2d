﻿using UnityEngine;

namespace src.Shooting
{
    public class LaserWeapon : Weapon
    {
        public LineRenderer lineRenderer;
        public Vector3 pos;

        private void Start()
        {
            lineRenderer.useWorldSpace = true;
        }

        private void Update()
        {
            lineRenderer.enabled = true;
            
            var angle = Mathf.Atan2(gunDirection.y, gunDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            
            pos = transform.position;
            pos.z = 0;
            lineRenderer.SetPosition(1, pos);
            lineRenderer.SetPosition(0, pointerPos);

            gunDirection.Normalize();
            ammunitionStartPoint = pos + gunDirection * 0.6f;
        }

        protected internal override void Shoot()
        {
            base.Shoot();

            float damage = 60;
            var hits = Physics2D.RaycastAll(ammunitionStartPoint, transform.TransformDirection(Vector3.down));
            
            foreach (var hit in hits)
                if (hit.collider != null)
                {
                    var tank = hit.transform.gameObject.GetComponent<Tank>();
                    if (tank != null) {
                        tank.TakeDamage(damage);
                    }
                }
            
            AudioSource.PlayClipAtPoint(Audio.boomClip, transform.position, 1);
            Messenger.Broadcast(GameEvent.ShotHit);
        }
    }
}