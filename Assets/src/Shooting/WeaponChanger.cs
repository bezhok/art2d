﻿using System.Collections.Generic;
using UnityEngine;

namespace src.Shooting
{
    public class WeaponChanger : MonoBehaviour
    {
        private int _weaponId;
        private List<Weapon> _weapons;

        private void Start()
        {
            _weapons = new List<Weapon>();
            var components = gameObject.GetComponents(typeof(Weapon));
            foreach (var comp in components) _weapons.Add((Weapon) comp);
            
        }

        internal void OnWeaponChanged()
        {
            _weaponId = (_weaponId + 1) % _weapons.Count;
            for (var i = 0; i < _weapons.Count; i++) _weapons[i].enabled = false;

            _weapons[_weaponId].enabled = true;
        }

        internal Weapon GetCurrentWeapon()
        {
            return _weapons[_weaponId];
        }

        internal void Reset()
        {
            _weaponId = -1;
            OnWeaponChanged();
        }
    }
}