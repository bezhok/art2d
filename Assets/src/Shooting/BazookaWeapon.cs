﻿using UnityEngine;

namespace src.Shooting
{
    public class BazookaWeapon : Weapon
    {
        public GameObject ammunitionPrefab;
        public LineRenderer line;
        private float _angle;

        protected virtual void Update()
        {
            if (line != null)
            {
                line.enabled = false;
            }
            
            ammunitionStartPoint = transform.position + gunDirection * 0.6f;
            _angle = Mathf.Atan2(gunDirection.y, gunDirection.x) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
        }
        
        private void Start()
        {
            var dir = new Vector3(-1, 1);
            dir.Normalize();
            ammunitionStartPoint = transform.position + dir * 0.6f;

            _angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

            gunDirection = dir;
            transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
        }

        protected internal override void Shoot()
        {
            base.Shoot();
            Instantiate(ammunitionPrefab, ammunitionStartPoint, transform.rotation);
        }
    }
}