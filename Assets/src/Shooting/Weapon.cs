using UnityEngine;

namespace src.Shooting
{
    public abstract class Weapon : MonoBehaviour
    {
        protected Vector3 ammunitionStartPoint;
        protected internal Vector3 gunDirection;
        protected Vector3 pointerPos;

        protected internal virtual void Shoot()
        {
            Messenger.Broadcast(GameEvent.ShotWasMade);
        }

        protected internal void UpdateGunDirection()
        {
            gunDirection = pointerPos - transform.position;
            gunDirection.Normalize();
        }

        protected internal void SetAimingPosition(Vector3 pos)
        {
            pointerPos = pos;
        }
    }
}