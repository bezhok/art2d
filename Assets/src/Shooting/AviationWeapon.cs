﻿using UnityEngine;

namespace src.Shooting
{
    public class AviationWeapon : BazookaWeapon
    {
        public LineRenderer lineRenderer;

        private void Start()
        {
            lineRenderer.useWorldSpace = true;
        }

        protected override void Update()
        {
            base.Update();
            lineRenderer.enabled = true;
            var lineStartPos = new Vector3(pointerPos.x, 200);
            var hit = Physics2D.Raycast(lineStartPos, Vector2.down);

            if (hit.collider != null)
            {
                lineRenderer.SetPosition(1, lineStartPos);
                lineRenderer.SetPosition(0, hit.point);
            }

            ammunitionStartPoint = new Vector2(lineStartPos.x, 5);
        }

        protected internal override void Shoot()
        {
            Messenger.Broadcast(GameEvent.ShotWasMade);

            var amm = Instantiate(ammunitionPrefab, ammunitionStartPoint, transform.rotation)
                .GetComponent<Ammunition>();
            
            amm.startPower = 0;
            amm.Launch();
        }
    }
}