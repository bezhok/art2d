﻿using UnityEngine;

namespace src
{
    public class Tank : MonoBehaviour
    {
        public float health = 100;
        public Transform healthBarUI;
        public bool isActive;
        public bool isDestroyed;
        private Vector3 _startScale;

        private void Start()
        {
            _startScale = healthBarUI.localScale;
        }

        internal void TakeDamage(float damage)
        {
            health -= damage;
            if (health <= 0)
            {
                DestroyTank();
            }
        }
        private void Update()
        {
            healthBarUI.localScale = new Vector3(_startScale.x * health / 100, _startScale.y);
            if (health <= 0)
            {
                DestroyTank();
            }
        }

        internal void DestroyTank()
        {
            isDestroyed = true;
            gameObject.SetActive(false);
        }
        
        internal void SetHealthColor(Color color)
        {
            healthBarUI.gameObject.GetComponent<SpriteRenderer>().color = color;
        }
    }
}