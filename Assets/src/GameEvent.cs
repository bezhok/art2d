﻿namespace src
{
    public static class GameEvent
    {
        public static string ChangeWeaponId = "ChangeWeaponId";
        public static string TimeRemaining = "TimeRemaining";
        public static string TimeOver = "TimeOver";
        public static string ShotWasMade = "ShotWasMade";
        public static string ShotHit = "ShotHit";
        public static string PlayerLost = "PlayerLost";
        public static string PlayerWatchedAd = "PlayerWatchedAd";
    }
}