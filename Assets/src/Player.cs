﻿using System.Collections.Generic;
using src.Shooting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace src
{
    public class Player : MonoBehaviour
    {
        private CharacterController2D _characterController2D;
        private Tank _tank;
        public Joystick joystick;
        public WeaponChanger weapon;
        // [SerializeField] private WeaponChanger _weapon;
        
        private void Start()
        {
            _characterController2D = GetComponent<CharacterController2D>();
            _tank = GetComponent<Tank>();
            
            Messenger.AddListener(GameEvent.ChangeWeaponId, () =>
            {
                if (_tank.isActive)
                {
                    weapon.OnWeaponChanged();
                }
            });
        }
        
        private bool IsPointerOverUIObject() {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
        
        private void Update()
        {
            if (_tank.isActive)
            {
                var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mPos.z = 0;

                var deltaX = Mathf.Sign(joystick.Horizontal) * _characterController2D.speed;
                if (Mathf.Abs(joystick.Horizontal) < 0.2f) deltaX = 0;
                _characterController2D.Move(deltaX);

                if (IsPointerOverUIObject()) return;
                weapon.GetCurrentWeapon().SetAimingPosition(mPos);
                weapon.GetCurrentWeapon().UpdateGunDirection();
            }
            else
            {
                _characterController2D.Move(0);
            }
        }

        public void Jump()
        {
            _characterController2D.Jump(new Vector2(Mathf.Sign(joystick.Horizontal), 1).normalized);
        }
        
        public void Shoot()
        {
            if (_tank.isActive)
                weapon.GetCurrentWeapon().Shoot();
        }
    }
}