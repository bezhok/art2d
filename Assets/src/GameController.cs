using System.Collections.Generic;
using System.Linq;
using src.UI;
using UnityEngine;

namespace src
{
    public class GameController : MonoBehaviour
    {
        private readonly List<Tank> _attackers = new List<Tank>();

        private int _currentAttackerId;
        public Tank enemy1;
        public Tank enemy2;
        public Tank player;
        public Timer timer;

        private void Start()
        {
            player.SetHealthColor(MainMenuUIController.GetColorByID(PlayerPrefs.GetInt("colorID")));
            _attackers.Add(player);
            _attackers.Add(enemy1);
            _attackers.Add(enemy2);

            Messenger.AddListener(GameEvent.TimeOver, OnTimeOver);
            Messenger.AddListener(GameEvent.ShotWasMade, OnShoot);
            Messenger.AddListener(GameEvent.ShotHit, OnTimeOver);
            Messenger.AddListener(GameEvent.PlayerWatchedAd, () =>
            {
                player.health = 50;
                player.isDestroyed = false;
                player.gameObject.SetActive(true);
                _currentAttackerId = _attackers.Count-1;
                OnTimeOver();
            });

            player.isActive = true;
        }

        private void OnShoot()
        {
            timer.Stop();
            _attackers[_currentAttackerId].isActive = false;
        }

        private void Update()
        {
            if (player.isDestroyed)
            {
                timer.Stop();
                Messenger.Broadcast(GameEvent.PlayerLost);
            }
        }

        private void OnTimeOver()
        {
            _attackers[_currentAttackerId].isActive = false;
            player.GetComponent<Player>().weapon.Reset();

            _currentAttackerId = (_currentAttackerId + 1) % _attackers.Count;

            var aliveCount = _attackers.Select(a => a.isDestroyed).Count(a => !a);
            if (aliveCount == 0)
            {
                timer.Stop();
                Messenger.Broadcast(GameEvent.PlayerLost);
                return;
            }

            if (_attackers[_currentAttackerId].isDestroyed) OnTimeOver();
            _attackers[_currentAttackerId].isActive = true;
            timer.Reset();
        }
    }
}