using UnityEngine;
using UnityEngine.Tilemaps;

namespace src
{
    public class TileDestoyer : MonoBehaviour
    {
        public static TileDestoyer instance;
        public Tilemap terrain;
    
        private void Awake()
        {
            instance = this;
        }
    
        public void Destroy(Vector3 position, float radius)
        {
            for (var x = -(int) radius; x < radius; x++)
            for (var y = -(int) radius; y < radius; y++)
            {
                var tilePos = terrain.WorldToCell(new Vector3(x, y, 0) + position);
                if (terrain.GetTile(tilePos) != null)
                {
                    DestroyTile(tilePos);
                }
            }
        }

        private void DestroyTile(Vector3Int tilePos)
        {
            terrain.SetTile(tilePos, null);
        }
    }
}