﻿using src.Shooting;
using UnityEngine;

namespace src
{
    public class EnemyAI : MonoBehaviour
    {
        private CharacterController2D _characterController2D;
        private Tank _tank;
        public Transform targetTankTransform;
        public Weapon weapon;

        private void Start()
        {
            _characterController2D = GetComponent<CharacterController2D>();
            _tank = GetComponent<Tank>();

            Messenger.AddListener<float>(GameEvent.TimeRemaining, (t) => {
                if (t < 1 && _tank.isActive)
                {
                    weapon.Shoot();
                }
            });
        }

        private float CalculateRequiredMovement()
        {
            var pos = transform.position;
            var bulletPos = pos.x - weapon.gunDirection.x * weapon.gunDirection.y * 200 / 198 ;
            
            return targetTankTransform.position.x - bulletPos;
        }

        private void FixedUpdate()
        {
            if (_tank.isActive)
            {
                var targetPosition = targetTankTransform.position;
                weapon.gunDirection =
                    new Vector3(targetPosition.x, targetPosition.y + 15) -
                    weapon.transform.position;
                
                weapon.gunDirection.Normalize();
                
                var deltaX = CalculateRequiredMovement();
                if (Mathf.Abs(deltaX) > 1.5)
                {
                    deltaX = Mathf.Sign(deltaX) * Time.deltaTime * _characterController2D.speed * 60;
                    _characterController2D.Move(deltaX);
                }
                else
                {
                    weapon.Shoot();
                }
            }
            else
            {
                _characterController2D.Move(0);
            }
        }
    }
}